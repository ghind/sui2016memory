TEXBASENAME = AVI16_Hind

PDFLATEX = $(shell which pdflatex)
BIBTEX = $(shell which bibtex)
OPEN = open

all: build open

# latex_files = $(wildcard *.tex) $(wildcard *.bib)

build: ${TEXBASENAME}.tex #$(latex_files)
	${PDFLATEX} ${TEXBASENAME}
	${BIBTEX} ${TEXBASENAME}
	${PDFLATEX} ${TEXBASENAME}
	${PDFLATEX} ${TEXBASENAME}

open:
	${OPEN} ${TEXBASENAME}.pdf

.PHONY: all build open
