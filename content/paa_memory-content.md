
# Introduction

\begin{figure*}[tbh]
 \centering
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth, width=0.65\columnwidth]{Paahighlight}
 \end{subfigure}%
 ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{Paahighlighthandpointing}
 \end{subfigure}
  ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{handheld}
 \end{subfigure}
 \caption{The three projection-based guidance techniques in the study: (a) an actuated pico-projector automatically highlights the target (system control), (b) an extension to the previous technique where the user adds kinesthetic feedback by pointing to the highlighted target, and (c) manual exploration with a hand-held pico-project that guides the user to the target.}
 \label{fig:concept}
\end{figure*}

Complex environments such as command and control rooms, warehouses, or libraries can contain thousands of objects, organized in a large physical space. Locating a given object in such environments can be difficult and time-consuming [@Smith2005]. Saving even a few seconds off of this task can result in significant cumulative savings. Thus, many tasks, such as maintenance or control room operating, require memorizing the location of many objects to be performed efficiently. Moreover, being able to retrieve objects quickly is of crucial importance for safety-critical tasks. While an interactive guidance system would help, such a system may be only available for training, either because equipping production sites would be too costly or for security reasons, where the operator must be able to master the system in any situation. Operators must then learn the positions of objects so as to rapidly find controls and avoid costly—or even dramatic—errors.

Guidance techniques have been proposed to help users to quickly and accurately find a given target, including using maps [@Arning2012], turn-by-turn instructions [@Mulloni2011; @Arning2012], augmented-reality glasses [@Henderson2009] or steerable projectors [@Butz2004; @Gacem2015]. Previous studies have focused on how to help people more quickly find targets [@Henderson2009; @Gacem2015; @Ishikawa2008], but they have not explored the impact of such guidance on spatial memory (\eg, learning the positions of objects). Furthermore, findings about memorization in navigation studies cannot be extended to spatial memory, because of (1) differences in the nature of the task [@Scarr2012] and (2) the non convergence of results. For examples, some of these studies have shown a positive influence of active vs. passive exploration on memorization [@Peruch1998; @Larrue2013], no effect [@Gaunet2001; @Wilson1999], or an inverse correlation [@Ishikawa2008].

In this paper, we explore the impact on memorization of three guidance techniques based on steerable or handheld projection.  We focus on such techniques, as proposed in [@Butz2004; @Gacem2015; @Cauchard2012; @Kratz2012; @Schoning2009; @Lochtefeld2010], because steerable projection has been shown to be particularly effective at helping users to quickly locate targets [@Gacem2015].

More specifically, through these three techniques, we investigate the role of (1) user versus system control of the guidance device and (2) kinesthetic feedback during object localization. The first technique transfers control to the system. It uses an implementation of Gacem \etal’s system-controlled robotic projection arm [@Gacem2015] which has been shown to help users quickly locate targets (Figure \ref{fig:concept}a). The second (Figure \ref{fig:concept}b) extends this approach to provide kinesthetic feedback by having users point at targets, as in [@Soechting1989].
For the third technique (Figure \ref{fig:concept}c), we propose a novel application of handheld projection based on [@Schoning2009; @Li2013] that guides the user to the target using visual cues.  

Results show that (1) the system-controlled technique had a higher recall rate than the manual guidance technique, (2) kinesthetic feedback of pointing with the arm does not seem to have an effect on long-term memorization for the system controlled condition, and (3) the visibility and noticeability of objects impact memorization.

# Related Work {#sec:related-work}

Various guidance systems have been proposed to help users find targets or navigate paths using a variety of different technologies, devices, and interaction styles. We focus primarily on projection-based guidance techniques (Section \ref{sec:projection-sys}) and user control in guidance and memorization in navigation and spatial positioning tasks (Section \ref{sec:user-control}).

## Projection Systems {#sec:projection-sys}

Projection-based guidance systems have been proposed either to expand the interaction space [@Cao2007] or the display space [@Cauchard2011; @Pinhanez2003; @Wilson2012] using motorized [@Pinhanez2003; @Wilson2012] or handheld devices [@Cao2007; @Cauchard2012; @Cauchard2011; @Lochtefeld2009]. These studies have considered such aspects as user acceptance [@Arning2012], temporal performance [@Gacem2015], user preference of projector orientation [@Cauchard2012], or spatial memory [@Kaufmann2013].

Butz \etal’s SearchLight [@Butz2004] uses a ceiling-mounted steerable projector to help users find objects by shining a spotlight on them. Gacem \etal [@Gacem2015] extend this approach by mounting a pico-projector on a robotic arm within the user’s field of view, finding that placing the projector within the user’s field of view reduced search times by up to 24% [@Gacem2015].

Li \etal evaluate handheld projection of maps and arrows to help guide users in wayfinding tasks [@Li2013], using a technique similar to our proposed handheld guidance technique.  In their technique, however, the user maintains a projection on a relatively fixed point: the floor in front.  In this way, our proposed handheld guidance technique is effectively a hybrid between this method and Schöning \etal's Map Torchlight [@Schoning2009], in which the user sweeps the projector over a map to highlight paths.  It does not, however, provide in-situ spatial guidance toward objects.  As such, it does not provide kinesthetic or "muscle memory" feedback for locating objects in space.  With the proposed Handheld Guidance technique, the user's movement to locate a target object is aligned with the physical movement necessary to locate it.

This approach is similar to Henderson & Feiner's task localization approach [@Henderson2009], which uses head-mounted augmented reality glasses.  As such, kinesthetic feedback is tied to where the user looks as opposed to where the user points, thus involving a more subtle for of kinesthetic interaction.

Little work has studied the role of spatial memory while using projection based guidance. Kaufmann \etal [@Kaufmann2013] studied the effect on memory in map navigation of displaying a map on a smartphone vs. projected in the environment with a handheld projector, finding up to a 41% improvement to spatial memory when using the projector. They hypothesize that this difference may be due to the kinetic component of moving the projector. To the best of our knowledge, no previous work has addressed the issue of spatial memory using motorized projection guidance. In this paper, we investigate how transferring control from the user to the system impacts the memorization of object locations.

## User vs. System Control {#sec:user-control}

With user-controlled guidance systems, guidance provided by the system depends on the user’s actions, whereas system-controlled techniques remove the user from much of the interaction loop. As such, system-controlled guidance (1) removes the kinesthetic feedback involved in moving the projector and (2) makes locating the object a more passive task. User-controlled guidance divides the user’s attention between controlling the device and learning the target’s location. Several studies have attempted to characterize the respective effects of user or system control in real or virtual navigation tasks, but we are not aware of such work on spatial positioning tasks.

Moreover, navigation studies in this area have not found conclusive results. Gaunet \etal [@Gaunet2001] found no significant difference between exploring a virtual city with a joystick and passively observing the scene on estimating the direction of the starting points, reproducing the shape of paths, or on scene recognition. This result seems to echo those of Wilson [@Wilson1999], suggesting that active exploration does not benefit spatial memorization.

Other studies, however, have found that active participants, who also controlled their movement with a joystick, better recalled spatial layout (\eg, finding the shortest path, drawing a map of the environment, or identifying starting positions of a path) than passive participants [@Brooks1999; @Attree1996; @Peruch1998]. In contrast, they did not find a significant effect on participants’ recall of the correct locations of objects in the virtual environment.

Larrue \etal [@Larrue2013] and Ruddle \etal [@Ruddle2011], on the other hand, found that user-controlled exploration yielded better memorization. Unlike the previous studies, however, these experiments involve richer body movement (body translation and rotation) and more complex environments. In particular, Larrue found that controlling rotation with the head led to higher quality spatial representations [@Larrue2013]. Such kinesthetic feedback may play a role in spatial memorization, as Soechting & Flanders observed that short-term memory accuracy improved when users pointed toward targets with the arms, either actively or passively [@Soechting1989].

Together, these results show that kinesthetic feedback, active or passive interaction, divided attention, and the complexity of the environment all may influence spatial memorization and may interact with each other. Moreover, such memorization may be different for paths and navigation than for learning the positions of objects [@Scarr2012].

We are not aware of prior work that explores the roles of kinesthetic feedback and of user vs. system control on spatial memorization of object positions in dense environments.

# Guidance System {#sec:guidance_systems}

We consider three different projector-based guidance conditions in this study. We designed each of them to be similar enough to be comparable, yet provide distinct properties in terms of control and kinesthetic feedback.

#### Robotic Arm (RA) {#sec:RA}
The robotic arm (RA) technique uses an implementation of Gacem \etal’s Projection-Augmented Robotic Arm technique [@Gacem2015] consisting of using a robotic arm with a pico-projector mounted on the end and attached to a small cart within the user’s field of view (Figure \ref{fig:ppa}). The arm automatically moves in the direction of a target and highlights it with a spotlight. The amount of time needed for to orient the arm was between 228 and 3800 ms depending on the distance between the initial position and the final position of the arm. To locate a target, participants could see not only the spotlight but also the orientation of the arm, which is always visible (possibly in peripheral vision) (Figure \ref{fig:concept}a). The arm thus indicates to the user the general direction of the target, and the spotlight indicates its precise position.

#### Robotic Arm + Kinesthetic Feedback (RAK) {#sec:RAK}
This second technique combines RA with kinesthetic feedback. RAK is similar to the previous technique except that, when the robotic arm points and highlights the target, participants must point the target with their arm (Figure \ref{fig:concept}b). This kind of kinesthetic feedback is similar to that proposed by Soechting \etal [@Soechting1989].

#### Handheld Guidance (HG) {#sec:HG}
We created a new Handheld Guidance (HG) technique to act as a baseline.  It is inspired by Yee's Peephole interaction technique [@Yee2003], Li \etal's indoor navigation technique [@Li2013], Schöning \etal's Map Torchlight [@Schoning2009], and Henderson & Feiner's task localization [@Henderson2009].  The user holds a pico-projector (the same model as in the previous conditions) in his or her hand (Figure \ref{fig:figurehandheld}a).  When the projector is far from the target, it displays an arrow that guides toward the target (Figure \ref{fig:figurehandheld}b).  The participant moves the projector in the direction indicated by the arrow until the target is within range of the projector.  The target is then highlighted, as in the previous techniques (Figure \ref{fig:figurehandheld}c).

These three techniques are each based on the same principle except that RA and RAK are system controlled while HG is user controlled. RA can be seen as HG performed by the robot instead of the user’s arm. Transferring control from the user to the robotic arm also removes kinesthetic feedback in the RA condition. The RAK technique reintroduces kinesthetic feedback through mid-air pointing by the user.

![The Robotic Arm (RA) technique. The robotic arm is initially oriented towards the door and displays the name of the target.\label{fig:ppa}](PAAName){width=45%}

# Study: User vs. System Control

As previously explained, various guidance techniques transfer control from the user to the system, which results in an absence of kinesthetic feedback. While, in general, body movement seems to have positive impact on memorization, previous studies are somewhat inconclusive as results differ depending on the experimental setup. Indeed, the way control is performed, the level of embodiment, the resulting level of divided attention, and the complexity of the task seem to strongly impact results.

We thus further investigated whether motorized projection-based guidance would hinder spatial memorization by considering a task that is inspired by a real use case in which operators are trained to find controls rapidly. This experimental setup involves a complex environment with many objects laid out on the walls of a room, including behind the user. For this purpose, we compared the RA, RAK, and HG conditions.

\begin{figure*}[tbh]
 \centering
 \begin{subfigure}[ ]
  \centering
  \includegraphics[width=0.65\columnwidth]{handheldname}
 \end{subfigure}%
 ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[width=0.65\columnwidth]{handheldarrow}
 \end{subfigure}
  ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[width=0.65\columnwidth]{handheldhighlight}
 \end{subfigure}
 
 \caption{Handheld guidance (HG): (a) the participant points the projector at the starting position to reveal the name of the next target. (b) The participant presses a button to start the trial, showing an arrow that points toward the target. (c) When the user moves the projector near the target, it is highlighted. The user presses a button to end the trial.}
 \label{fig:figurehandheld}
\end{figure*}

## Experimental Design

### Participants and Apparatus

#### Participants
42 subjects (20 female), aged 21 to 34 (mean $= 27.2$, SD $= 3.57$), participated in the study. To avoid hidden confounds, we selected participants with a similar profile (engineering students from the same institution, with no HCI background) and controlled for gender across groups. We also performed a Guilford-Zimmerman test of spatial knowledge acquisition [@Kyritsis2009], which showed that the spatial abilities of participants were similar between groups ($p=0.79$). Participants were compensated with candies and, for each technique, the participant who memorized the most targets received a bonus box of chocolates.

![(a) The participant is standing at the center of the room. (b) A wall and objects in the room.\label{fig:figureroom}](Room){width=49%}


#### Setting
The experiment took place on three walls of a room. Each wall measured approximately $2 \times 5$ meters (about $6.5 \times 16.5$ feet) and was located in front of, behind, or to the side of the participant (Figure \ref{fig:figureroom}). The room was under standard overhead fluorescent lighting with the curtains drawn so as to maintain consistent lighting conditions. Figure \ref{fig:figureroom}a shows a top view of the room with a participant at the starting position in the center of the room.

#### Projection
The projector’s field of view was large enough to provide a sufficiently large peephole (about 1.2 m diagonal) to make framing a reasonable task and avoid disturbing participants.

#### Robotic Arm Speed
Previous findings have shown that guidance with a robotic arm reduces execution time [@Gacem2015]. To reduce a bias introduced by such timing differences, we set a slow movement speed on the robotic arm in order to more closely align time on task for the RA, RAK, and HG conditions.

#### Targets
A set of 1143 targets were printed on paper posters affixed to the three walls (Figure \ref{fig:figureroom}b). They were equally distributed across the three walls, on a surface of $4.2 \times 1.8$ m for each wall. The design of the targets and their layout on the walls (Figure \ref{fig:figmetruefalse}) was inspired from the control panels of an actual power plant. Their size ranged from $4 \times 4$ cm to $12 \times 12$ cm (more detail below) and the distance between their borders was variable.

While some distractors were only 5cm apart, all targets used during the experiment were at least at 8cm from their nearest neighbor in order to allow participants to unambiguously select targets and to let us focus on memorization rather than pointing accuracy.

Target labels were drawn from the industrial domain and consisted of simple words (\eg, Pump, Voltage, Engine, \etc.). These labels were projected at the same place at the beginning of each trial to inform about the target (Figure \ref{fig:ppa}). They were not printed on posters to avoid to bias the results of the experiment. Moreover, this situation mimics real conditions where labels are too small to be readable from a distance.

We took into account the position of targets, which were either in front of, beside, or behind the user, which thus affected target visibility. We also distinguished two levels of _noticeability_. In contrast with other targets, those that were different from neighbors, isolated, or close to a landmark (such as a door) were considered _easier-to-notice_. These targets made up 50% of those used in the experiment. We used this categorization to provide a rough approximation of the distinctiveness or noticeability of targets.

As detailed below, participants were instructed to learn twelve targets, randomized between easy- or less-easy-to-notice targets and across the different walls (in front of, beside, or behind the user).

![Memory test: (a) the hand-held pico-projector (HG) displays a cursor to the user. (b) A visual feedback (true or false) indicates if the selected item is correct.\label{fig:figmetruefalse}](truefalse){width=49%}


### Method and Procedure

Because skill-transfer and interference is not well understood in such cases, memorization tasks commonly use between-subjects designs to avoid possible interaction effects (e.g. [@Kaufmann2013; @Attree1996; @Ruddle2011; @Larrue2013]). In particular, our experiment involves two similar techniques (RA & RAK) and a third baseline (HG). Even counterbalancing the order would probably favor RA & RAK, so we used a between-subjects design with a relatively large number of participants (42) randomly assigned to three groups. Over the course of any given day, we ran one group (so as to counter-balance time-of-day effects such as post-lunch lethargy and to minimize possible interaction effects).

In summary, we evaluated the RA, RAK, and HG conditions in a between-subject test and the two characteristics of the targets (*position* and *noticeability*) in a within-subjects design. In total we had 6452 trials.

The experiment was divided into two sessions. The first session lasted approximately 30 minutes, and the second session lasted about 5 minutes. As shown in Figure \ref{fig:figuresession}, the first session involved four successive training and testing phases. The second session, performed 48 hours later, involved a single (long-term) memory test.

![The experiment was performed over two sessions. The first session was composed of four successive training and testing blocks. The second session consisted in a memory test performed two days later.\label{fig:figuresession}](session){width=49%}

#### Training phase
During this phase, participants were asked to find, as quickly as possible, 12 randomly ordered targets and to memorize their locations. The same list of targets was used along the experiment. For each technique, the same starting position was used, either through the system (RA & RAK) or by requiring the user to point the projector at the start position to reveal the stimulus (HG). The name of the desired target was then displayed by the projector (Figure \ref{fig:figurehandheld}a). The participant would then start the searching phase by clicking on a button, which would make the target name disappear.

In RA and RAK conditions, the robotic arm then started moving towards the direction of the target. In HG condition, a directional arrow appeared (Figure \ref{fig:figurehandheld}b) and the participant could start moving the handheld projector according to the direction of the arrow. In all cases, the target was highlighted when the projection window reached the target (Figure \ref{fig:figurehandheld}c and \ref{fig:concept}a). In the RAK condition, the participant also then pointed to the highlighted target with his or her arm. Participants could then take some time to memorize the target before moving on to the next trial.

#### Memory Test
Participants were instructed to find as accurately as possible the 12 targets they previously localized, but without any guidance. Twelve trials were performed in this phase (one for each target). Participants first had to point the projector on the same initial position to start a trial. As before, this caused the name of the target to be displayed. Participants were required to look at this name for one second before they could then click on the start button. This made the  target name disappear and the projected cursor appear (Figure \ref{fig:figmetruefalse}a). Participants could then move the projector to place the cursor on the target of their choice, then confirm by pressing again the same button. The system then provided feedback about whether the selected target correctly matched the stimulus (Figure \ref{fig:figmetruefalse}b).

<!-- ### Measures
\jre{R1: ``The experimental design requires a paragraph to explain all the
variables of the experiment, classified in independent and dependent,
and how they are measured''}

For each test trial, we recorded whether the participant indicated a correct target and the distance from the selected target and the correct target.  We also recorded the time taken to identify a target.  This time was measured from when the user dismissed the stimulus until the user pressed a button to end the trial.  Additionally, we recorded how long the stimulus was shown, the movement time, and the highlight time spent by participants looking at the target. -->

<!-- *stimulus-time*, the duration in which the name of the target is displayed, (b) *movement-time*, the duration in which the projector moves towards the target (either automatically or by the user’s movement), and (c) *highlight-time*, the amount of time spent by participants looking at the highlighted target before starting the next trial. -->


### Hypotheses

Because the two conditions with robotic arm guidance (RA & RAK) reduce kinesthetic feedback, our hypotheses were that: 
<!-- R1: ``Hypotheses paragraph should be moved to following the Experimental Design, and considers in their definitions the independent variables of the experiment'' -->

1.  Participants would (a) learn more targets (b) more quickly with handheld guidance (HG) than when transferring control to the robotic arm (RA & RAK).
2.  Adding kinesthetic feedback (RAK) when using the robotic arm (RA) should improve memorization because we expect “muscle memory” to play a positive role in learning.
3.  Regardless the guidance technique, memorization should be higher for targets located in front of the user, because building a mental model should be easier for targets that are always visible.
4.  Participant would memorize a larger number of more noticeable targets.


## Results

![Mean recall rates. The first four groups correspond to the memory tests during the first session. The last group corresponds to the retention test in the second session, 48 hours later.\label{fig:figmeananswer}](recall_rate_exp1){height=35% width=49%}

### Memorization

We used recall rate to evaluate memorization performance, that is the percentage of targets correctly identified by participants during the testing phases.

![Trial exploration time for each technique, by block.\label{fig:figbarcharttrialtime}](ExplorationTime){height=37% width=48%}

#### Memory Performance During Training
The training phase included four memory tests, as illustrated in Figure \ref{fig:figuresession}. We performed a two-way ANOVA test on the recall rates of these memory tests and found a statistically significant difference between the four testing blocks ($F_{3,117}=177, p<0.01$). Participants were increasingly identified correct targets as the experiment progressed (Figure \ref{fig:figmeananswer}). Recall rates increased by 39%, 13% and 6% between successive blocks for RA, by 33%, 14% and 3.75% for RAK, and by 29%, 19% and 19% for HG. Hence, the initial slope of the learning curve was quite high when using the robotic arm (RA and RAK), with more than two-thirds of the targets properly identified by the second test. In comparison, the learning curve was lower for the handheld technique (HG).

We also found a statistically significant difference in recall rate between techniques ($F_{2,39}=9.74, p<0.01$). Participants memorized targets more accurately when using the robotic arm (RA $F_{1,26}=16, p<0.01$ and RAK $F_{1,26}=13, p<0.01$)
than with the handheld device (HG). There was no significant difference between RA and RAK.

During training, the interaction effect between techniques and blocks was statistically significant ($F_{6,117}=2.36, p<0.05$). A post-hoc Tukey test showed statistically significant results for the first three test blocks between RAK and HG and between RA and HG groups. RA Participants learned more than HG participants, and RAK participants learned more correct targets than HG. However, we found no statistically significant difference between techniques for the fourth block, probably because values were already quite high (Table \ref{tab:tablememo1}).
  
\begin{table}
\centering
\caption{Percentage of memorized targets per technique and per memory testing block}\label{tab:tablememo1}
\begin{tabular}{|c|c|c|c|c|c|c|} \hline
Techniques& block1& block2& block3& block4&Mean\\ \hline
RA & 37\% & 77\% & 91\% & 97\% & 91\%\\ \hline
RAK & 42\% & 75\% & 90\% & 94\% & 86\%\\ \hline
HG & 17\% & 47\% & 66\% & 86\% & 72\%\\
\hline\end{tabular}
\end{table}

#### Performance During the Retention Test
We performed a one-way ANOVA test on the recall rate over the final memory test, performed two days later. We found a statistically significant difference between techniques ($F_{2,39}=6.4, p<0.05$). Again, participants memorized targets more accurately when using RA ($F_{1,26}=26.11, p<0.01$) and when using RAK ($F_{1,26}=5.21, p<0.01$) versus HG. There was no significant difference between RA and RAK.

#### Target Position and Noticeability
We performed a three-way ANOVA test on the recall rates for block test, position, and noticeability of targets. We found a statistically significant difference between “easy-” and “less-noticeable” targets ($F_{1,156} = 7.77, p<0.01$). Unsurprisingly, in the beginning of the experiment, recall rates were higher for “easy-to-notice” targets (36.1% vs. 28.1%) and this was still the case at the end of the experiment (94% vs. 81%). In the retention test, recall rates were also higher for “easy-to-notice” targets (87% vs. 80%).

We also found a statistically significant difference between targets located in front of and behind the participants ($F_{1,156} = 8.97, p<0.01$). The recall rate of targets behind the user was lower in the beginning of the experiment (26.5% vs. 38.4%) but similar at the end (92% vs. 93%). No significant effect was found for the retention test.

\begin{figure*}
    \centering
    \includegraphics[height=0.2\textheight,width=0.8\columnwidth]{traj1}
    \hspace{4em}
    \includegraphics[height=0.2\textheight,width=0.8\columnwidth]{traj2}
    \caption{A sample training trial for one user (a) at the beginning the experiment (block 1) and (b) at the end of the experiment (block 4). The plots show the distance between the center of the projected area and the target. RA is shown in red, RAK in green, and HG in blue. The thick dot on each curve indicates the moment when the target was first highlighted.}
    \label{fig:trajectory}
\end{figure*}


### Training Time

We performed a two-way ANOVA of the task time spent in each of the four training blocks, finding a statistically significant difference between blocks ($F_{3,117}=34, p<0.001$). Unsurprisingly, time decreased across blocks as illustrated in Figure \ref{fig:figbarcharttrialtime}.

We found no statistically significant difference on task time between techniques ($p = 0.4$), although the amount of time was slightly shorter on average for RA and RAK than for HG (Table \ref{tab:tabletime1}).

\begin{table}
\centering
\caption{Trial Time in (s) for each technique and training block}\label{tab:tabletime1}
\begin{tabular}{|c|c|c|c|c|c|c|} \hline
Techniques& block1& block2& block3& block4&Mean\\ \hline
RA & 7.7s & 7.1s & 5.8s & 5.6s & 6.5s\\ \hline
RAK & 8.1s & 6.2s & 5.3s & 5.0s & 6.1s\\ \hline
HG & 9.5s & 8.5s & 6.6s & 5.3s & 7.5s\\
\hline\end{tabular}
\end{table}

As differences in performance did not seem related to global training time duration, we performed a deeper analysis by decomposing trials time into three parts: (a) _stimulus-time_, the duration in which the name of the target is displayed, (b) _movement-time_, the duration in which the projector moves towards the target (either automatically or by the user’s movement), and (c) _highlight-time_, the amount of time spent by participants looking at the highlighted target before starting the next trial.

A two-way ANOVA of _highlight-time_ showed a statistically significant difference between blocks ($F_{3,117}=18, p<0.001$). Participants spent less time while advancing in the experiment for all techniques (Figure \ref{fig:figbarcharttrialtime}).

A two-way ANOVA of _highlight time_ also showed a statistically significant difference between techniques ($F_{2,39} = 5.13, p<0.05$). Participants from two robotic arm techniques spent less time (2 seconds for RA and 1.9s for RAK) looking at the highlighted target than those holding the projector (3s). Surprisingly, the group with lower memorization performance (HG) is thus the one that spent more time looking at the highlighted target.

### Distance Curves

Figure \ref{fig:trajectory} shows a sample training trial performed (a) at the beginning and (b) at the end of the experiment. Comparing the plots, we can see that training time considerably decreased at the end of the experiment, as each curve shifts left. The blue curve (HG) in Figure \ref{fig:trajectory}a shows an important fluctuation as the user overshoots the target. This fluctuation is highly attenuated at the end of the experiment.

The small oscillations at the end of the RA and RAK plots are the results of the abrupt stop of servomotors.

## Discussion

#### User vs. system control (**H1**)
Transferring control from the handheld guidance (HG) technique to the two robotic arm conditions makes the user more passive and reduces the time spent finding each target. As such, we expected participants to learn fewer target positions in the RA condition than in the HG condition, and that the benefits of kinesthetic feedback would put the RAK condition between the two.

The results show, however, that H1 did not hold: participants memorized more targets in less time in the RA & RAK conditions than in the HG condition. We suspect that this difference may be the result of dividing the user’s attention between controlling the device and locating the object, as is the case in [@Gaunet2001; @Wilson1999].

Observations of participants during training phases seem to support this theory. Participants who explored the room manually (HG) seemed to be more concentrated on manipulating the projector before and while the target was highlighted. RA & RAK participants, however, tended to concentrate only on the highlighted target, more naturally and effortlessly locating the target.

Participants’ subjective judgements also seem to support this hypothesis. At the end of the first session, we asked participants to rate cognitive effort, frustration, and subjective preference for each of the different guidance techniques on a 7-point Likert scale. We found no differences between techniques for frustration and preference, but participants found RA and RAK less cognitively demanding than HG (6.42 for RA, 6.5 for RAK and 5.57 for HG, higher scores are better). This difference may result from a higher level of divided attention.

#### Kinesthetic feedback and spatial memory (**H2**)
Participants in the HG condition experience a kinesthetic feedback as they search for the target. We hypothesized that this feedback would reinforce memorization through some kind of “muscle memory,” echoing results by Kaufmann \etal [@Kaufmann2013]. In the RA condition, however, participants do not experience such kinesthetic feedback. We thus expected that kinesthetic feedback would lead to the outcomes predicted by H1: HG would provide the strongest support for memorization, RA would underperform, and RAK would be between the two as it combines RA with a form of kinesthetic feedback. As described above, however, this kinesthetic feedback did not seem to have much impact when the robotic arm was used: RA and RAK produced similar results, both superior to HG. Thus, H2 does not hold.

This results contrasts with the findings of most previous studies on navigation tasks [@Soechting1989; @Wraga2003; @Peruch1998]. However, as indicated in Section \ref{sec:related-work}, some other studies (e.g., [@Gaunet2001; @Wilson1999]) found no significant difference between exploring a virtual environment using a joystick and passively observing the scene. Hence, results may depend on various differences between studies, such as the kind of body movement, the complexity of the environment, the number of target objects, the inclusion of distractors, or the type of spatial memory involved. For example, in motor-based tasks (\eg, [@Larrue2013; @Ruddle2011]), it makes sense that motor feedback would enhance path or layout memorization.

That we observed no significant difference between RA & RAK conditions may be due to the fact that in complex localization tasks (\eg, learning 12 targets among 1200 objects), the task is primarily visual rather than motor-based and thus might not benefit as much from kinesthetic feedback. Moreover, in the RAK condition, kinesthetic feedback only occurred when the user was asked to point at the target, hence after the target was found. This constrasts with tasks where the user must continuously control a device during the whole search task, such as is the case in the HG condition.  

These conflicting results for different kinds of spatial memory on different kinds of tasks and in different kinds of contexts call out the need for more work in this domain. Moreover, the fact that H1 & H2 did not hold suggests that more studies are needed to better understand the roles of, \eg, the complexity of the context and task, of divided attention, and of types of kinesthetic feedback on spatial memorization. This work presents an important set of data points toward this understanding.

Lastly, that H1 & H2 were invalidated is encouraging for such actuated or robotized guidance systems. It suggests not only that system-controlled guidance can help to more quickly identify the locations of targets, but also that users are better able to learn those positions than with a handheld, user-controlled guidance technique. In future work it may be interesting to compare such guidance to no guidance at all.

#### Target characteristics (**H3** & **H4**)
As expected, targets that are easy to notice and that are always visible were memorized faster than those that were overloaded with distractors or those occasionally visualized. One possible explanation, is that permanent visibility of a target helps the development of a mental model of the environment and *easy-to-notice* targets provide enough hints to allow users to create a mental mapping of their positions.


# Conclusions

People typically need to learn and memorize objects in training scenarios, to make them able to interact with (or maintain) complex environments such as plants or control rooms. The main motivation of this study was a real use case where operators are trained to rapidly find controls.

Guidance techniques help people to more quickly and more easily locate objects in complex environments, but their effect on learning is less well understood. In this paper, we compared several guidance techniques in order to evaluate the impact of projection-based guidance techniques on spatial memorization. We compared two system-controlled guidance techniques that use a robotic projection arm and a new user-controlled, projected peephole technique called Handheld Guidance. A major difference between these two cases is that the first technique transfers control to the system, reducing the active involvement of the user, and is also likely to allow the user focusing on memorization.

We expected that the system-controlled guidance techniques, which help participants to more quickly and more easily identify the positions of targets, would hinder learning. Instead, we found that participants were able to learn the positions of more objects and more quickly with the robotic projection arm than with manual, handheld guidance. This suggests that such guidance techniques may be useful in training contexts, where learning is important and where merely finding objects is insufficient.

Our outcomes extend prior findings on spatial memory in positioning tasks, revealing that active control on its own is insufficient to improve memorization performance. We suspect that this result is due to the division of the user’s attention between memorization and controlling the device. Similarly, we found that kinesthetic feedback does not necessarily improve spatial memory. Future work should study the the role of division of attention, task alignment, and more precisely identifying the role kinesthetic feedback can play in spatial memorization.
