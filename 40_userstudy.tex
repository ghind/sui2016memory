%!TEX root = AVI16_Hind.tex
\section{Study: User vs. System Control}

As previously explained, various guidance techniques transfer control from the user to the system, which results in an absence of kinesthetic feedback. While, in general, body movement seems to have positive impact on memorization, previous studies are somewhat inconclusive as results differ depending on the experimental setup. Indeed, the way control is performed, the level of embodiment, the resulting level of divided attention, and the complexity of the task seem to strongly impact results.

We thus further investigated whether motorized projection-based guidance would hinder spatial memorization by considering a task that is inspired by a real use case in which operators are trained to find controls rapidly. This experimental setup involves a complex environment with many objects laid out on the walls of a room, including behind the user. For this purpose, we compared the RA, RAK and HG conditions.

\subsection{Hypotheses}
Because the two conditions with robotic arm guidance (RA \& RAK) reduce kinesthetic feedback, our hypotheses were that: 
\begin{itemize}
 \setlength{\itemsep}{1pt}
 \setlength{\parskip}{0pt}
 \setlength{\parsep}{0pt}
 \item[\textbf{H1}] Participants would (a) learn more targets (b) more quickly with handheld guidance (HG) than when transferring control to the robotic arm (RA \& RAK).
 \item[\textbf{H2}] Adding kinesthetic feedback (RAK) when using the robotic arm (RA) should improve memorization because we expect ``muscle memory'' to play a positive role in learning. \hg{According to Soechting \etal\cite{Soechting1989} and Larrue \etal~\cite{Larrue2013}, the kinesthetic feedback such as pointing in simple localization task \cite{Soechting1989} or navigation in virtual scene with body movement \cite{Larrue2013} can enhance learning.}

 \item[\textbf{H3}] Regardless the guidance technique, memorization should be higher for targets located in front of the user, because building a mental model should be easier for targets that are always visible.
 \item[\textbf{H4}] Memorization should depend on the noticeability of targets (as explained below).
\end{itemize}

\begin{figure*}[tbh]
 \centering
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{figures/handheldname.pdf}
 \end{subfigure}%
 ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{figures/handheldarrow.pdf}
 \end{subfigure}
  ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{figures/handheldhighlight.pdf}
 \end{subfigure}
 
 \caption{Handheld guidance (HG): (a) the participant points the projector at the starting position to reveal the name of the next target. (b) The participant presses a button to start the trial, showing an arrow that points toward the target. (c) When the user moves the projector near the target, it is highlighted. The user presses a button to end the trial.}
 \label{fig:figurehandheld}
\end{figure*}

\subsection{Experimental Design}
\subsubsection{Participants and Apparatus}

\textit{Participants}.
42 subjects (20 female), aged 21 to 34 (mean = 27.2, SD = 3.57), participated in the study. To avoid hidden confounds, we selected participants with a similar profile (engineering students from the same institution, with no HCI background) and controlled for gender across groups. We also performed a Guilford-Zimmerman test of spatial knowledge acquisition \cite{Kyritsis2009}, which showed that the spatial abilities of participants were similar between groups (p=0.79). Participants were compensated with candies and, for each technique, the participant who memorized the most targets received a bonus box of chocolates.


\textit{Setting}.
The experiment took place on three walls of a room. Each wall measured approximately 2 $\times$ 5m (about 6.5 $\times$ 16.5~ft) and was located in front of, behind, or to the side of the participant (Figure~\ref{fig:figureroom}). The room was under standard overhead fluorescent lighting with the curtains drawn so as to maintain consistent lighting conditions. Figure~\ref{fig:figureroom}a shows a top view of the room with a participant at the starting position in the center of the room.

\textit{Projection}.
The projector's field of view was large enough to provide a sufficiently large peephole (about 1.2 m diagonal) to make framing a reasonable task and avoid disturbing participants.

\textit{Robotic Arm Speed}.
Previous findings have shown that guidance with a robotic arm reduces execution time \cite{Gacem2015}. To reduce a bias introduced by such timing differences, we set a slow movement speed on the projector in order to more closely align time on task for the RA, RAK, and HG conditions.

%\gb{\textit{RA \& RAK implementation}.}
%\gb{RA and RAK rely on Gacem et al’s system \cite{} and use a picoprojector mounted on a robotic arm. However, we used  a more robust MX-64T servo motor to increase the precision (0.089$\deg$ instead of 0.35$\deg$). We also changed the structure of the arm to provide a 360° projection field (instead of XXX). We reused the motion capture system (ARTTRACK) and the predefined model of the environment from \cite{} to be able to calculate, from the position and orientation of the device and the projected surface, the transformation to apply to the projection.}

%\hg{\textit{RA \& RAK Setup}.}
%\hg{As stated earlier (section3), system used in RA \& RAK techniques is reproduction of Gacem et al’s system[8] which consist of a picoprojector mounted on a robotic arm. This system is based on infrared measurement system (ARTTRACK),PC, input device (keybord) and a robotic arm. Their tracking methode use a predefined model of the environment(including spatial postion of targets) and an a closed loop control system.}

%\hg{In this study, we implemented a new and improved version of "PAA" \cite{Gacem2015}. We have used more robust servo motors MX-64T with better precision 0.089° instead of 0.35°. We have also changed the structure of the arm to provide a 360° projection field. Concerning the input device, we have replaced the keyboard with a single push button which is much less cumbersome.}

%\hg{\textit{HG Setup}.}
%\hg{Except the robotic arm, the hardware components used for HG are similar to those used for RA and RAK. To calculate the direction of the virtual arrow, first, the position of the target is computed in projection frame of reference. The resulting spherical coordinates of are then transformed into  Cartesian coordinates in the projection plane. This transformation determines the direction of the arrow. In real time, the direction of the arraw is updated.}

\begin{figure}[tbh]
 \centering
 \includegraphics[height =0.6\columnwidth,width=1\columnwidth]{figures/Room.jpg}
 \caption{(a) The participant is standing at the center of the room. (b) A wall and objects in the room.}
 \label{fig:figureroom}
\end{figure}

\textit{Targets}.
A set of 1143 targets were printed on paper posters affixed to the three walls (Figure~\ref{fig:figureroom}b). They were equally distributed across the three walls, on a surface of $4.2 \times 1.8$ m for each wall. The design of the targets and their layout on the walls (Figure \ref{fig:figmetruefalse}) was inspired from the control panels of an actual power plant. Their size ranged from $4 \times 4$ cm to $12 \times 12$ cm (more detail below) and the distance between their borders was variable.

While some distractors were only 5cm apart, all targets used during the experiment were at least at 8cm from their nearest neighbor in order to allow participants to unambiguously select targets and to let us focus on memorization rather than pointing accuracy.

Target labels were drawn from the industrial domain and consisted of simple words (\eg, Pump, Voltage, Engine, etc.). These labels were projected at the same place at the beginning of each trial to inform about the target (Figure~\ref{fig:ppa}). They were not printed on posters to avoid to bias the results of the experiment. Moreover, this situation mimics real conditions where labels are too small to be readable from a distance.

We took into account the position of targets, which were either in front, beside, or behind the user, which thus affected target visibility. We also distinguished two levels of \textit{noticeability}. In contrast with other targets, those that were different from neighbors, isolated, or close to a landmark (like a door) were considered \textit{easier-to-notice}. These targets made up 50\% of those used in the experiment. We used this categorization to provide a rough approximation of the distinctiveness or noticeability of targets.

As detailed below, participants were instructed to learn a sequence of 12 targets, randomized between easy- or less-easy-to-notice targets and across the different walls (in front of, beside, or behind the user).

\begin{figure}[tbh]
\centering
 \includegraphics[trim=0 2cm 0 4.5cm, clip, height =0.6\columnwidth, width=1\columnwidth]{figures/truefalse.jpg}
% \includegraphics[width=1\columnwidth]{figures/truefalse.jpg}
 \caption{Memory test: (a) the hand-held pico-projector (HG) displays a cursor to the user. (b) A visual feedback (true or false) indicates if the selected item is correct.}
 \label{fig:figmetruefalse}
\end{figure}


\subsubsection{Method and Procedure}

Because skill-transfer and interference is not well understood in such cases, memorization tasks commonly use between-subjects designs to avoid possible interaction effects (e.g. \cite{Attree1996, Kaufmann2013, Larrue2013, Ruddle2011}). In particular, our experiment involves two similar techniques (RA \& RAK) and a third baseline (HG). Even counterbalancing the order would probably favor RA \& RAK, so we used a between-subjects design with a relatively large number of participants (42) randomly assigned to three groups. Over the course of any given day, we ran one group (so as to counter-balance time-of-day effects such as post-lunch lethargy and to minimize possible interaction effects).

In summary, we evaluated the RA, RAK, and HG conditions in a between-subject test and the two characteristics of the targets (\textit{position} and \textit{noticeability}) in a within-subjects design. In total we had 6452 trials.

The experiment was divided into two sessions. The first session lasted approximately 30 minutes, and the second session lasted about 5 minutes. As shown in Figure~\ref{fig:figuresession}, the first session involved four successive training and testing phases. The second session, performed 48 hours later, involved a single (long-term) memory test.

\begin{figure}[tbh]
\centering
 \includegraphics[trim=0 0cm 0 0cm, width=1\columnwidth,clip]{figures/session.pdf}
 \caption{The experiment was performed over two sessions. The first session was composed of four successive training and testing blocks. The second session consisted in a memory test performed two days later.}~\label{fig:figuresession}
\end{figure}

\textit{Training phase}.
During this phase, participants were asked to find, as quickly as possible, a sequence of 12 randomly ordered targets and to memorize their locations. The same list of targets was used along the experiment. For each technique, the same starting position was used, either through the system (RA \& RAK) or by requiring the user to point the projector at the start position to reveal the stimulus (HG). The name of the desired target was then displayed by the projector (Figure~\ref{fig:figurehandheld}a). The participant would then start the searching phase by clicking on a button, which would make the target name disappear.

In RA and RAK conditions, the robotic arm then started moving towards the direction of the target. In HG condition, a directional arrow appeared (Figure~\ref{fig:figurehandheld}b) and the participant could start moving the handheld projector according to the direction of the arrow. In all cases, the target was highlighted when the projection window reached the target (Figure~\ref{fig:figurehandheld}c and \ref{fig:concept}a). In the RAK condition, the participant also then pointed to the highlighted target with his or her arm. Participants could then take some time to memorize the target before moving on to the next trial. 

\textit{Memory Test}.
Participants were instructed to find as accurately as possible the 12 targets they previously localized, but without any guidance. Twelve trials were performed in this phase (one for each target). Participants first had to point the projector on the same initial position to start a trial. As before, this caused the name of the target to be displayed. Participants were required to look at this name for one second before they could then click on the start button. This made the target name disappear and the projected cursor appear (Figure~\ref{fig:figmetruefalse}a). Participants could then move the projector to place the cursor on the target of their choice, then confirm by pressing again the same button. The system then provided feedback about whether the selected target correctly matched the stimulus (Figure~\ref{fig:figmetruefalse}b).
