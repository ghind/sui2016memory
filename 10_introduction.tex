%!TEX root = AVI16_Hind.tex

\section{Introduction}
\begin{figure*}[tbh]
 \centering
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth, width=0.65\columnwidth]{figures/Paahighlight.jpg}
 \end{subfigure}%
 ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{figures/Paahighlighthandpointing.jpg}
 \end{subfigure}
  ~ 
 \begin{subfigure}[ ]
  \centering
  \includegraphics[height =0.6\columnwidth,width=0.65\columnwidth]{figures/handheld.jpg}
 \end{subfigure}
 \caption{The three projection-based guidance techniques in the study: (a) an actuated pico-projector automatically highlights the target (system control), (b) an extension to the previous technique where the user adds kinesthetic feedback by pointing to the highlighted target, and (c) manual exploration with a hand-held pico-project that guides the user to the target.}
 \label{fig:concept}
\end{figure*}

Complex environments such as command and control rooms, warehouses, or libraries can contain thousands of objects, organized in a large physical space. Locating a given object in such environments can be difficult and time-consuming~\cite{Smith2005}. Saving even a few seconds off of this task can result in significant cumulative savings. Thus, many tasks, such as maintenance or control room operating, require memorizing the location of many objects to be performed efficiently. Moreover, being able to retrieve objects quickly is of crucial importance for safety-critical tasks. While an interactive guidance system would help, such a system may be only available for training, either because equipping production sites would be too costly or for security reasons, where the operator must be able to master the system in any situation. Operators must then learn the positions of objects so as to rapidly find controls and avoid costly -- or even dramatic -- errors.

Guidance techniques have been proposed to help users to quickly and accurately find a given target, including using maps~\cite{Arning2012}, turn-by-turn instructions~\cite{Arning2012,Mulloni2011}, augmented-reality glasses~\cite{Henderson2009} or steerable projectors~\cite{Butz2004,Gacem2015}. Previous studies have focused on how to help people more quickly find targets~\cite{Gacem2015,Henderson2009,Ishikawa2008}, but they have not explored the impact of such guidance on spatial memory (\ie, learning the positions of objects). Furthermore, findings about memorization in navigation studies cannot be extended to spatial memory, because of (1) differences in the nature of the task~\cite{Scarr2012} and (2) the non convergence of results. For examples, some of these studies have shown a positive influence of active vs. passive exploration on memorization~\cite{ Larrue2013,Peruch1998}, no effect~\cite{Gaunet2001, Wilson1999}, or an inverse correlation~\cite{Ishikawa2008}.

In this paper, we study the impact on memorization of guidance techniques based on steerable or handheld projection. We focus on such techniques, as proposed in~\cite{Butz2004,Cauchard2012,Gacem2015,Kratz2012,Lochtefeld2010,Schoning2009}, because steerable projection has been shown to be particularly effective at helping users to quickly locate targets~\cite{Gacem2015}.

\gb{More specifically, we investigate the role of (1) user versus system control of the guidance device and (2) kinesthetic feedback during object localization by comparing three techniques.} The first technique transfers control to the system. It uses an implementation of Gacem \etal's system-controlled robotic projection arm~\cite{Gacem2015} which has been shown to help users quickly locate targets (Figure~\ref{fig:concept}a). The second (Figure~\ref{fig:concept}b) extends this approach to provide kinesthetic feedback by having users point at targets \cite{Soechting1989}. The third (Figure~\ref{fig:concept}c) uses a handheld projector (similar to~\cite{Schoning2009}) that guides the user to the target using directional hints and acts as a baseline.

Results show that (1) the system-controlled technique had a higher recall rate than the manual guidance technique, (2) kinesthetic feedback of pointing with the arm does not seem to have an effect on long-term memorization for the system controlled condition, and (3) the visibility and noticeability of objects impact memorization.
